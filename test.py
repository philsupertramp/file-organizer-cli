import typing
from enum import Enum
import abc
import json


class BaseEnum(Enum):
    def __init__(self, *args):
        self.verbose_name, self._value_ = args[:2]
        self._value2member_map_[self._value_] = self

    @classmethod
    def get_choices(cls):
        return tuple((i._value_, i.verbose_name) for i in cls)

    @classmethod
    def get_name(cls, id):
        return cls(id).name

    @classmethod
    def get_id(cls, name):
        for i in cls:
            if i.verbose_name == name:
                return i._value_
        return None

    @classmethod
    def get_internal(cls, name):
        for i in cls.get_choices():
            if i[0] == name:
                return i[1]


class FileTypes(BaseEnum):
    JSON = 1, '.json'


class FileError(BaseException):
    def __init__(self, file_name, file_type):
        super().__init__('{} is not a valid {} file.'.format(file_name, file_type))


class JSONFileError(FileError):
    def __init__(self, file_name):
        super().__init__(file_name, FileTypes.JSON.value)


class WrongFormatError(BaseException):
    def __init__(self, detail: str = ''):
        super().__init__('Your content has the wrong format.{}'.format(' {}'.format(detail)))


class FileProcessor(abc.ABC):
    content = None

    def __init__(self, file_name: str = None, target_name: str = None) -> None:
        self.file_name = file_name
        self.target_name = target_name

    def read(self) -> object:
        """
        opens a file of type n
        :return: content of file
        """
        raise NotImplementedError

    def write(self, file_name: str = None):
        """
        writes a file of type n
        """
        raise NotImplementedError


class JsonFileProcessor(FileProcessor):
    file_type = FileTypes.JSON

    def read(self):
        try:
            self.content = json.loads(open(self.file_name).read())
        except json.JSONDecodeError:
            raise JSONFileError(self.file_name)
        return self.content

    def write(self, target_name: str = None):
        if target_name:
            self.target_name = target_name
        try:
            json.dump(self.content, open(self.target_name, 'w'), default=lambda x: x.__dict__())
        except TypeError:
            raise WrongFormatError(detail=self.file_type.verbose_name)


class DjangoContent:
    """
    Content storage for django model entity
    """
    model = None
    pk = None
    fields = None

    def __init__(self, model: str = None, pk: int = None, fields: typing.List = list):
        self.model = model
        self.pk = pk
        self.fields = fields

    def __str__(self):
        return '<DjangoContent model="{}" pk="{}">'.format(
            self.model if self.model else 'None',
            self.pk if self.pk else 'None'
        )

    def __dict__(self):
        """
        Serialization method
        :return:
        """
        return {
            'model': self.model or None,
            'pk': self.pk or None,
            'fields': self.fields or {}
        }


class DjangoContentList:
    """
    Content Manager for django model objs
    """
    objects = []
    objects_dict = {}
    fields_set = set()
    __iterator = 0

    def as_dict(self):
        """
        :return: content as dict
        """
        return self.objects_dict

    def as_list(self):
        """
        :return: content as list
        """
        return self.objects

    @property
    def length(self):
        """
        :return: num elem
        """
        return len(self.objects) - 1

    def exclude(self, app: str = None, model: str = None, pk: int = -1):
        """
        content exclusion filter
        :param app: app label
        :param model: model label
        :param pk: entities primary key
        :return: filtered list
        """
        if pk == -1:
            if model:
                new_list = [i for i in self.objects if i.model != model]
                new_dict = {i: v for i, v in self.objects_dict.items() if i != model}
                self.objects_dict = {}
                self.objects = []
                self.objects = new_list
                self.objects_dict = new_dict.copy()
                return self
            elif app:
                new_list = [i for i in self.objects if i.model.find(app) == -1]
                new_dict = {i: v for i, v in self.objects_dict.items() if i.find(app) == -1}
                self.objects_dict = {}
                self.objects = []
                self.objects = new_list
                self.objects_dict = new_dict.copy()
                return self

    def filter(self, app: str = None, model: str = None, pk: int = -1):
        """
        content filter
        :param app: app label
        :param model: model label
        :param pk: entities primary key
        :return: filtered list
        """
        if pk == -1:
            new_list = [i for i in self.objects if i.model == model]
            new_dict = {i: v for i, v in self.objects_dict.items() if i == model}
            self.objects_dict = {}
            self.objects = []
            self.objects = new_list
            self.objects_dict = new_dict.copy()
            del new_dict, new_list

    def append(self, obj) -> None:
        """
        :param obj:
        :return:
        """
        self.objects.append(obj)
        try:
            self.objects_dict[obj.model].append(obj)
        except KeyError:
            self.objects_dict[obj.model] = [obj]

        self.fields_set.add(field for field in obj.fields.keys())

    def __str__(self):
        return '<DjangoContentList count={} models={} fields={}>'.format(
            self.length,
            self.objects_dict.keys(),
            self.fields_set
        )

    def __len__(self):
        return self.length

    def __iter__(self):
        """

        :return:
        """
        self.__iterator = 0
        return self

    def __next__(self):
        """

        :return:
        """
        if self.__iterator < self.length:
            self.__iterator += 1
            return self.objects[self.__iterator]
        else:
            raise StopIteration


class ClientAction:
    def __init__(self, name=None, method=None, help_text=None, params=None):
        self.name = name
        self.method = method
        self.help_text = help_text
        self.params = params


class ClientActionMixin:
    actions = {
        'h': ClientAction(name='help', method='', help_text='Display this help', params=['h', '?']),
        's': ClientAction(name='save', method='_save_action', help_text='Save into file', params=['s', 'S', 'save']),
        'l': ClientAction(name='list', method='_list_action', help_text='List models|apps', params=['l', 'L', 'list']),
        'd': ClientAction(name='delete', method='_delete_action', help_text='Delete model|app', params=['d', 'D']),
        'q': ClientAction(name='quit', method='', help_text='Quit', params=['q', 'Q', 'exit', 'quit']),
    }

    def get_action(self, input_param):
        for action in self.actions.values():
            if input_param in action.params:
                return action

    def call(self, action: ClientAction):
        getattr(self, action.method)()

    def _delete_app_action(self):
        """

        :return:
        """
        quit = False
        while not quit:
            self._list_apps_action()
            try:
                model = int(input('Chose app to delete (insert id|-1 to exit): '))
            except (TypeError, ValueError):
                quit = True
            if quit or model in [-1]:
                return
            apps = list(self.apps)
            try:
                self.file_content = self.file_content.exclude(app=apps[model])
            except IndexError:
                print('Not found!')
                continue
            del apps[model]
            self.apps = set(apps)

        self._update_models()

    def _delete_model_action(self):
        """

        :return:
        """
        quit = False
        while not quit:
            self._list_models_action()
            try:
                model = int(input('Chose model to delete (insert id|-1 to exit): '))
            except (TypeError, ValueError):
                quit = True
            if quit or model in [-1]:
                return
            models = list(self.models)
            try:
                self.file_content = self.file_content.exclude(app=models[model])
            except IndexError:
                print('Not found!')
                continue
            del models[model]
            self.models = set(models)

        self._update_apps()

    def _save_action(self):
        """

        :return:
        """
        target_file_name = input('Target file name [incl. path]: ')
        self.file_processor.content = self.file_content.as_list()
        self.file_processor.write(target_file_name)

    def _update_apps(self):
        """

        :return:
        """
        self.apps = set()
        for app in self.models:
            self.apps.add(app.split('.')[0])

    def _list_models_action(self):
        """

        :return:
        """
        i = 0
        for model in self.models:
            print('{}: {}'.format(i, model))
            i += 1

    def _list_apps_action(self):
        """

        :return:
        """
        if not self.apps:
            self._update_apps()

        i = 0
        for app in self.apps:
            print('{}: {}'.format(i, app))
            i += 1

    def _update_models(self):
        """

        :return:
        """
        self.models = set()
        for obj in self.file_content:
            self.models.add(obj.model)

    def _list_action(self):
        list_type = input('Which type [A = app | M = model]: ')
        if list_type in ['A', 'a', 'app', 'App']:
            self._list_apps_action()
        elif list_type in ['M', 'm', 'Model', 'model']:
            self._list_models_action()

    def _delete_action(self):
        delete_type = input('Which type [A = app | M = model]: ')
        if delete_type in ['A', 'a', 'app', 'App']:
            self._delete_app_action()
        elif delete_type in ['M', 'm', 'Model', 'model']:
            self._delete_model_action()
        elif delete_type in ['']:
            pass


class Client(ClientActionMixin):
    FILE_PROCESSORS = {
        FileTypes.JSON: JsonFileProcessor
    }
    input_type = None
    file_name = None
    target_name = None
    file_processor = None
    file_content = DjangoContentList()
    raw_file_content = None
    fields = set()
    models = set()
    apps = None

    @property
    def __file_processor(self):
        """

        :return:
        """
        return self.FILE_PROCESSORS[self.input_type]

    def __process_content(self):
        """

        :return:
        """
        if isinstance(self.raw_file_content, list):
            for instance in self.raw_file_content:
                self.models.add(instance['model'])
                self.fields.add(key for key in instance['fields'].keys())
                obj = DjangoContent(model=instance['model'], pk=instance['pk'], fields=instance['fields'])
                self.file_content.append(obj)

    def __process_file(self, file_name, file_type) -> None:
        """

        :param file_name:
        :param file_type:
        :return:
        """
        self.input_type = FileTypes(file_type)
        self.file_name = file_name
        self.file_processor = self.__file_processor(file_name)
        self.raw_file_content = self.file_processor.read()
        self.__process_content()

    def read_file(self, filename: str):
        """

        :param filename:
        :return:
        """
        self.__process_file(file_name=filename, file_type='.{}'.format(filename.split('.')[-1]))

    def manage_actions(self):
        """

        :return:
        """
        chose_action = True
        while chose_action:
            action = input('Chose action ([?|h] for help): ')
            if action in ['?', 'h']:
                for action in self.actions.values():
                    print('{:<30}: {}'.format(str(action.params), action.help_text))
                continue
            elif action in ['q', 'Q', 'exit', 'quit']:
                break
            action_obj = self.get_action(action)
            if action_obj:
                self.call(action_obj)
                continue


def main():
    while True:
        name = input('File name: ')
        client = Client()
        client.read_file(filename=name)
        client.manage_actions()


if __name__ == '__main__':
    main()

