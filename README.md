Soon to be file extension manager. 
- [x] Edit django `.json` export using `./manage.py dumpdata > [filename].json`
    - can also edit other `.json` files with schema (see underneath)
- [x] interactive cli elements
- [ ] Convert `.json` files as required through cli
- [ ] Convert and edit `.css` files

## for usage type `[h|?]`

#### currently supported schema

```json
[{"model": [string value], "pk": [int value], "fields": {"[field name]": [string value]}},...]
```