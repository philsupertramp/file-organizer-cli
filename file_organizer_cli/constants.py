from .utils.enum import BaseEnum


class FileTypes(BaseEnum):
    JSON = 1, '.json'
