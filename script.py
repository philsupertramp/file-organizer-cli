from file_organizer_cli.client import Client


def main():
    while True:
        name = input('File name: ')
        client = Client()
        client.read_file(filename=name)
        client.manage_actions()


if __name__ == '__main__':
    main()
